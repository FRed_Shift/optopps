# OptoPPS

This hardware item is intended to:

- supply a precise 1PPS (from the GPS) optical pulse, transmitted via an optical fiber to the sensor of the camera and finally form a pseudo star blinking at a rate of 1 pulse per second.
- supply the necessary USB signal (using the DCD capability) to be used with the NTP monitor application (from Meinberg) and aimed at synchronizing the PC clock with the PPS signal.

The supply current is sunk from the USB link.


2 faint LED (for nighly use) show the GPS frame (green LED, unused in this application but proves the GPS is OK) and the PPS pulse (red LED). 

The intensity of the projected blinking star can be adjusted with 2 buttons (+ and -) to match the observed occulted star (as usually, the goal is to avoid any saturation of the sensor).

The factory default pulse width of the PPS signal is often set to 100ms.
However, a 400ms pulse should be used to widden the use to all the possible occultation times.

2 versions were evaluated:

- with the internal GPS antenna. Works perfectly for an outdoor use. Minimizes the wire-management...
- with an external GPS antenna. If you like wires.

BTW, this pulse width must match the one defined in the Siril prefs when using the occultation resync feature (https://gitlab.com/free-astro/siril/-/merge_requests/671).


A few ser files are available in the in the following link to test the Siril capability.
https://www.dropbox.com/scl/fi/w5bj9ndbzzqt5o956pfra/20240419_firstOccult_nosync.zip?rlkey=4f0eb3yin9ruco4be4qiw7p49&st=cmu4plpg&dl=0

![Alt text](20240322_120229.jpg)